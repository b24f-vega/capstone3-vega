import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Container,Row,Col,Card,Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { format } from 'date-fns';
import { DayPicker } from 'react-day-picker';
import UserContext from "../UserContext";
import useLoader from "../hooks/useLoader";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
export default function ServiceView(){
  const {user} = useContext(UserContext)
  const [name,setName] = useState(``);
  const [description,setDescription] = useState(``);
  const [price,setPrice] = useState(``);
  const [image,setImage] = useState(``)
  const{serviceId} = useParams();
  const [selectedDay, setSelectedDay] = useState();
  const [loader, showLoader, hideLoader] = useLoader();
  const navigate = useNavigate();
  // Get Service Description
  useEffect(() => {
    showLoader();
    fetch(`${process.env.REACT_APP_API_URL}/service/${serviceId}`)
    .then(result => result.json())
    .then(data =>{
      console.log(data)
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setImage(data.image);
        hideLoader();
    })
  },[serviceId]);
  //Avail Service
  const availService = (id) =>{
    showLoader()
    fetch(`${process.env.REACT_APP_API_URL}/user/availService/${id}`,{
        method:'POST',
        headers:{
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin': '*',
            Authorization:`Bearer ${localStorage.getItem('token')}`
        },
        body:JSON.stringify({
          scheduledOn:selectedDay
      })
    })
    .then(result => result.json())
    .then(data => {
      hideLoader();
      console.log(data)
      if(data === true){
        Swal.fire({
          title:'Transaction Successfull!',
          icon:'success',
        })
        navigate('/UserDashboard')
      }
      else{
        Swal.fire({
          title:'Transaction Failed',
          icon:'error',
          text: "Please try again!"
      })
      }

    })
  }


  const footer = selectedDay ? (
    <p>You selected {format(selectedDay, 'PP')}.</p>
  ) : (
    <p>Please pick a day.</p>
  );
  return(
    <motion.div initial={{opacity:0}}
        animate={{opacity:1}}
        exit={{opacity:0}}>
      <Container className="mt-5 col-10 py-5 bg-light rounded " fluid>
          <Row>
                <Col className='text-dark col-xs-8 col-md-6 me-auto me-md-auto text-center'>
                <img variant="top" src={image} className='serviceImage my-1'/>
                <h5>{name}</h5>
                <p>Description:</p>
                <p>{description}</p>
                <p>Price: P {price}</p>
                </Col>		

              <Col className="text-dark col-xs-10 col-md-6 ms-md-auto">	
              <h4 className="text-center">Cleaning Schedule</h4>
                <div className="col-6 mx-auto ">
                <DayPicker
                  mode="single"
                  selected={selectedDay}
                  onSelect={setSelectedDay}
                  footer={footer}
                  className='mx-auto text-center'
                />
                </div>
                </Col>
                <Button variant="primary" as={Link} to='/Services' className='col-4 mx-auto my-4'>Go back</Button>
                <Button variant="primary" onClick = {()=> availService(serviceId)} className='col-4 mx-auto my-4'>Avail Service</Button>
              </Row>
      </Container>
    </motion.div>
  )
}