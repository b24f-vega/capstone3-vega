import React, { useState } from "react";
import { Carousel, Row,Col,Button } from "react-bootstrap";
import { useContext,useEffect } from "react";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";
import useLoader from '../hooks/useLoader'
function ServiceCarousel(serviceProp) {
    const{_id,isActive} = serviceProp
    const {user}= useContext(UserContext);
    const [serviceData,setServiceData] = useState([])
    const [loader, showLoader, hideLoader] = useLoader();
    useEffect(()=>{
      showLoader();
        fetch(`${process.env.REACT_APP_API_URL}/service/getActiveService`)
        .then(result => result.json())
        .then(data =>{
          hideLoader()
                setServiceData(data)
        })
        
    },[])
  return (
    <div className="py-5">
      <Carousel>
        {serviceData.map(service => (
          <Carousel.Item key={service._id} >
            <Row>
                <Col className="col-xs-8 col-md-6 mx-auto">
                    <div className='text-dark col-xs-8 col-md-6 me-auto me-md-auto'>
                        <img className="serviceImage  mx-auto" src={service.image} alt={service.name}/>
                    </div>
                </Col>
                <Col className="">
                    <Carousel.Caption>
                        <div className='text-dark col-xs-10 col-md-6 ms-md-auto '>
                            <h2 >{service.name}</h2>
                            <h3 >{service.price}</h3>
                            <p >{service.description}</p>
                            {
                                user?
                                <Button as = {Link} to = {`/service/${service._id}`} >See more details</Button>
                                :
                                <Button as={Link} to='/Login' >See more details</Button>
                            }   
                        </div>
                    </Carousel.Caption>
                </Col>
            </Row>
          </Carousel.Item>
        ))}
      </Carousel>
    </div>
  );
}

export default ServiceCarousel;