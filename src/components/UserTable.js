import { useEffect } from "react";
import { useState } from "react";
import useLoader from "../hooks/useLoader";
import { Table,Button, } from "react-bootstrap";
import { motion } from "framer-motion";
export default function UserTable({cancelBooking}){
    const [booking,setBooking] =useState([])
    const [loader, showLoader, hideLoader] = useLoader();
    useEffect(()=>{
      showLoader();
        fetch(`${process.env.REACT_APP_API_URL}/user/checkServices`,{
          headers:{
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin': '*',
            Authorization:`Bearer ${localStorage.getItem('token')}`
        }}
        )
        .then(result => result.json())
        .then(data =>{
                hideLoader();
                setBooking(data)
        })
        
    },[])
    function refreshBooking(event){
      event.preventDefault();
      showLoader();
      fetch(`${process.env.REACT_APP_API_URL}/user/checkServices`,{
        headers:{
          'Content-Type':'application/json',
          'Access-Control-Allow-Origin': '*',
          Authorization:`Bearer ${localStorage.getItem('token')}`
      }}
      )
      .then(result => result.json())
      .then(data =>{
              hideLoader();
              setBooking(data)

      })
    }
    
    return(
        <motion.div className="py-5" 
        initial={{opacity:0}}
        animate={{opacity:1}}
        exit={{opacity:0}}>
        <Button className="px-3 col-1 offset-11" onClick={(event)=>{refreshBooking(event)}}>Refresh</Button>
        <Table>
            <thead>
                <tr>
                    <th>Service Name </th>
                    <th>Schedule </th>
                    <th>Status</th>                 
                </tr>
            </thead>
            {booking.map(data => (
            <tbody key={data._id} >
                <tr>
                    <td>{data.serviceName}</td>
                    <td>{data.scheduledOn}</td>
                    <td>{data.status}</td>
                    <td>
                    {
                      data.status === "Pending"?
                      <Button className="mx-auto px-auto" onClick={(event) => {cancelBooking(data._id)}}>Cancel</Button>
                      :
                      null
                    }    
                    </td>
                </tr>
            </tbody>
            ))}
        </Table>
        
        </motion.div>
    )
}