import { useContext,useState } from "react"
import { useParams } from "react-router-dom";
import UserContext from "../UserContext"
import { Container,Row,Col,Button,Modal,Form, } from "react-bootstrap"
import ServiceTable from '../components/ServiceTable'
import Swal from "sweetalert2"
import useLoader from '../hooks/useLoader'
import { motion } from "framer-motion";
export default function AdminDashboard(){
        const {user} = useContext(UserContext)
        const [loader, showLoader, hideLoader] = useLoader();
        const{serviceId} = useParams();
        const [name,setName] = useState('')
        const [price,setPrice] = useState('')
        const [description,setDescription] = useState('')
        const [image,setImage] = useState('')
        const [formShow, setFormShow] = useState(false);
        const [services,setServices] = useState(null)
        const [editMode,setEditMode] = useState(false)  
        


        const handleClose = () => {
            setFormShow(false)
        };
        const handleShow = () => {
            setEditMode(false)
            setFormShow(true)
        };
        function addService (event){
            showLoader();
            fetch(`${process.env.REACT_APP_API_URL}/service/addService`,{
                method: 'POST',
                headers:{
                    'Content-Type' : 'application/json',
                    Authorization:`Bearer ${localStorage.getItem('token')}`
                },
                body:JSON.stringify({
                    name : name,
                    price :price,
                    description:description,
                    image : image
                })})
                .then(result => result.json())
                .then(data =>{
                    
                    if(data === false){
                        hideLoader();
                        Swal.fire({
                            title: "Failed!",
                            icon: "error",
                            text: "Please try again!"
                        })
                    }
                    else{
                        hideLoader();
                        Swal.fire({
                            title: "Service Added Successfully!",
                            icon: 'success', 
                        })
                        handleClose();
                    }
                })
                .catch(error => console.log(error))
        }
        
        function archiveService(serviceId){
            showLoader();
            fetch(`${process.env.REACT_APP_API_URL}/service/archiveService/${serviceId}`,{
                method: 'PUT',
                headers:{
                    'Content-Type':'application/json',
                    Authorization:`Bearer ${localStorage.getItem('token')}`
                }}).then(result => result.json())
                .then(data =>{
                    console.log(data)
                    if(data === false){
                        hideLoader();
                        Swal.fire({
                            title: "Failed!",
                            icon: "error",
                            text: "Please try again!"
                        })
                    }
                    else{
                        hideLoader();
                        Swal.fire({
                            title: "Service Archived Successfully!",
                            icon: 'success', 
                        })
                    }
                })
                .catch(error => console.log(error))
        }
        function editService(services){
            showLoader();
            setEditMode(true);
            setServices(services);
            setFormShow(true)
            console.log(services)
            fetch(`${process.env.REACT_APP_API_URL}/service/updateService/${services._id}`,{
                method:'PUT',
                headers:{
                    'Content-Type':'application/json',
                    Authorization:`Bearer ${localStorage.getItem('token')}`
                },
                body:JSON.stringify({
                    name : name,
                    price :price,
                    description:description,
                    image : image
                })
            }).then(result => result.json())
            .then(data =>{
                console.log(data)
                if(formShow){
                    if(data === false){
                        hideLoader();
                        Swal.fire({
                            title: "Failed!",
                            icon: "error",
                            text: "Please try again!"
                        })
                    }
                    else{
                        hideLoader();
                        Swal.fire({
                            title: "Service Updated Successfully!",
                            icon: 'success', 
                        })
                        handleClose();
                    }
                }
            })
            .catch(error => console.log(error))
        }
    return(
        
        <motion.div initial={{opacity:0}}
        animate={{opacity:1}}
        exit={{opacity:0}}>
        
         <h1 className='mt-3 text-center text-light'>Admin Dashboard</h1>
            <Container fluid className=" bg-light rounded">
                
                    <h2 className="col-xs-10 mx-auto px-2 text-center pt-5">Services</h2>
                <Row>
                <Col >
                    <Button onClick={handleShow} to='/AdminDashboard' className="col-1 offset-11 my-3 px-4">Add Service</Button>
                    <ServiceTable archiveService = {archiveService} editService={editService}/>
                </Col>
                </Row>
            
            </Container>
            <Modal
                size="lg"
                show={formShow}
                onHide={() => setFormShow(false)}
                aria-labelledby="example-modal-sizes-title-lg"
                centered
            >
                <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">
                    Add Service
                </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter new Service Name"
                            value={name}
                            onChange={event => setName(event.target.value)}
                            autoFocus
                        />
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter new Service Price"
                            autoFocus
                            value={price}
                            onChange={event => setPrice(event.target.value)}
                        />
                        </Form.Group>
                        <Form.Group
                        className="mb-3"
                        controlId="exampleForm.ControlTextarea1"
                        >
                        <Form.Label>Image URL</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter new Service Image URL"
                            autoFocus
                            value={image}
                            onChange={event => setImage(event.target.value)}
                        />
                        <Form.Label>Details</Form.Label>
                        <Form.Control 
                            as="textarea" 
                            rows={3}
                            value={description}
                            onChange={event => setDescription(event.target.value)}
                             />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose} className='py-3'>
                        Close
                    </Button>
                    <Button variant="primary" onClick={editMode?(event) => editService(services) : (event) => addService(serviceId)} type='submit' >
                    {
                        editMode? 'Update User'
                        :
                        'Add New Service'
                    }
                    </Button>
                </Modal.Footer>
            </Modal>
         </motion.div>
    )
}
