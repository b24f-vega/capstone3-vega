import Banner from "../components/Banner.js";
import { Container } from "react-bootstrap";
import { motion } from "framer-motion";
export default function Home(){
    return(
        <motion.div 
            initial={{opacity:0}}
            animate={{opacity:1}}
            exit={{opacity:0}}
        >
            <Container>
                <Banner/>
            </Container>
        </motion.div>
    )
}